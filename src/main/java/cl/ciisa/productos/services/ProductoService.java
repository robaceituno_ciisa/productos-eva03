package cl.ciisa.productos.services;

import java.util.List;
import java.util.logging.Logger;

import cl.ciisa.productos.model.entities.Producto;
import cl.ciisa.productos.model.dao.ProductoDAO;
import cl.ciisa.productos.model.dao.exceptions.NonexistentEntityException;

public class ProductoService {

    ProductoDAO dao = new ProductoDAO();

    public List<Producto> traerProductos(){
        Logger.getLogger(this.getClass().getName()).info("Service::traerProductos");
        return dao.findProductoEntities();
    }

    public Producto buscarProducto(Long id){
        Logger.getLogger(this.getClass().getName()).info("Service::buscarProducto::" + id);
        return dao.findProducto(id);
    }

    public Long nuevoProducto(cl.ciisa.productos.model.domain.Producto prd) throws NonexistentEntityException, Exception {
        Logger.getLogger(this.getClass().getName()).info("Service::nuevoProducto::" + prd.getId());
        return dao.create(mapper(prd)).getId();
    }

    public Long guardarProducto(cl.ciisa.productos.model.domain.Producto prd) throws NonexistentEntityException, Exception {
        Logger.getLogger(this.getClass().getName()).info("Service::guardarProducto::" + prd.getId());
        return dao.edit(mapper(prd)).getId();
    }

    public void eliminarProducto(Long id) throws NumberFormatException, NonexistentEntityException {
        Logger.getLogger(this.getClass().getName()).info("Service::eliminarProducto::" + id);
        dao.destroy(id);       
    }

    private Producto mapper(cl.ciisa.productos.model.domain.Producto producto){
        Producto prd=new Producto();
        prd.setId(producto.getId());
        prd.setNombre(producto.getNombre());
        prd.setComplemento(producto.getComplemento());
        prd.setUnidad(producto.getUnidad());
        prd.setStock(producto.getStock());
        return prd;
    }

}