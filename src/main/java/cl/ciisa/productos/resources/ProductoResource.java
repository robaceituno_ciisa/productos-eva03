package cl.ciisa.productos.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cl.ciisa.productos.model.domain.ErrorResponse;
import cl.ciisa.productos.model.domain.MessageResponse;
import cl.ciisa.productos.model.domain.Producto;
import cl.ciisa.productos.services.ProductoService;

/**
 *
 * @author mono
 */
@Path("/productos")
public class ProductoResource { 

    ProductoService svc = new ProductoService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarProductos(){
        try{
            return Response
                .ok(svc.traerProductos())
                .build();
        } catch (Exception e){
            Logger.getLogger(this.getClass().getName()).severe("ERROR::" + e.getMessage());;
            ErrorResponse error = new ErrorResponse("500", e.getLocalizedMessage(), e.getMessage());
            return Response
                        .status(Status.INTERNAL_SERVER_ERROR)
                        .entity(error)
                        .build();
        }

    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarProductoId(@PathParam("id") Long id){
        Object obj = svc.buscarProducto(id);
        if ( obj==null ){
            ErrorResponse error = new ErrorResponse("404", "Producto no existe", "");
            return Response
                        .status(Status.NOT_FOUND)
                        .entity(error)
                        .build();
        }

        return Response
                .ok(svc.buscarProducto(id))
                .build();
    }  
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response crearProducto(Producto producto){
        try{
            Long id = svc.nuevoProducto(producto);
            MessageResponse msg = new MessageResponse(id, "Producto", "Producto creado correctamente.");
            return Response
                    .ok(msg)
                    .build();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).severe("ERROR::" + e.getMessage());;
            ErrorResponse error = new ErrorResponse("500", e.getLocalizedMessage(), e.getMessage());
            return Response
                        .status(Status.INTERNAL_SERVER_ERROR)
                        .entity(error)
                        .build();
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response actualizarProducto(Producto producto){
        Object obj = svc.buscarProducto(producto.getId());
        if ( obj==null ){
            ErrorResponse error = new ErrorResponse("404", "Producto no existe", "");
            return Response
                        .status(Status.NOT_FOUND)
                        .entity(error)
                        .build();
        }

        try{
            Long id = svc.guardarProducto(producto);
            MessageResponse msg = new MessageResponse(id, "Producto", "Producto actualizado correctamente.");
            return Response
                    .ok(msg)
                    .build();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).severe("ERROR::" + e.getMessage());;
            ErrorResponse error = new ErrorResponse("500", e.getLocalizedMessage(), e.getMessage());
            return Response
                        .status(Status.INTERNAL_SERVER_ERROR)
                        .entity(error)
                        .build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response borrarProductoId(@PathParam("id") Long id){
        Object obj = svc.buscarProducto(id);
        if ( obj==null ){
            ErrorResponse error = new ErrorResponse("404", "Producto no existe", "");
            return Response
                        .status(Status.NOT_FOUND)
                        .entity(error)
                        .build();
        }

        try{
            svc.eliminarProducto(id);
            MessageResponse msg = new MessageResponse(id, "Producto", "Producto eliminado correctamente.");
            return Response
                    .accepted(msg)
                    .build();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).severe("ERROR::" + e.getMessage());
            ErrorResponse error = new ErrorResponse("500", e.getLocalizedMessage(), e.getMessage());
            return Response
                        .status(Status.INTERNAL_SERVER_ERROR)
                        .entity(error)
                        .build();
        }
    }  
    
}
