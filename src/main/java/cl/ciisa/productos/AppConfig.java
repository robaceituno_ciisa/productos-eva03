package cl.ciisa.productos;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

/**
 * Configures JAX-RS for the application.
 * @author mono
 */

@ApplicationPath("/api")
@OpenAPIDefinition(info = @Info(
        title = "Evaluación de Unidad 3", 
        version = "1.0.0", 
        contact = @Contact(
                name = "Robinson Aceituno Campos", 
                email = "robinson.aceituno.campos@ciisa.cl",
                url = "")
        ),
        servers = {
            @Server(url = "/",description = "localhost")
        }
        
)
public class AppConfig extends Application {
    
}
