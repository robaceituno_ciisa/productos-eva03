/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.productos.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import cl.ciisa.productos.model.entities.Producto;
import cl.ciisa.productos.model.dao.exceptions.PreexistingEntityException;
import cl.ciisa.productos.model.dao.exceptions.NonexistentEntityException;

/**
 *
 * @author mono
 */
public class ProductoDAO implements Serializable {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("db");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public ProductoDAO(final EntityManagerFactory emf) {
        this.emf = emf;
    }

    public ProductoDAO() {

    }

    public Producto create(final Producto producto) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(producto);
            em.getTransaction().commit();
        } catch (final Exception ex) {
            if (findProducto(producto.getId()) != null) {
                throw new PreexistingEntityException("Producto " + producto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return producto;
    }

    public Producto edit(Producto producto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            producto = em.merge(producto);
            em.getTransaction().commit();
        } catch (final Exception ex) {
            final String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                final Long id = producto.getId();
                if (findProducto(id) == null) {
                    throw new NonexistentEntityException("The producto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return producto;
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto;
            try {
                producto = em.getReference(Producto.class, id);
                producto.getId();
            } catch (final EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The producto with id " + id + " no longer exists.", enfe);
            }
            em.remove(producto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Producto> findProductoEntities() {
        return findProductoEntities(true, -1, -1);
    }

    public List<Producto> findProductoEntities(final int maxResults, final int firstResult) {
        return findProductoEntities(false, maxResults, firstResult);
    }

    private List<Producto> findProductoEntities(final boolean all, final int maxResults, final int firstResult) {
        final EntityManager em = getEntityManager();
        final CriteriaQuery cq = emf.getCriteriaBuilder().createQuery();
        final Root<Producto> rt = cq.from(Producto.class);
        try {
            final Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Producto findProducto(Long id) {
        final EntityManager em = getEntityManager();
        try {
            return em.find(Producto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoCount() {
        final EntityManager em = getEntityManager();
        final CriteriaQuery cq = emf.getCriteriaBuilder().createQuery();
        final Root<Producto> rt = cq.from(Producto.class);
        try {
            cq.select(em.getCriteriaBuilder().count(rt));
            final Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
