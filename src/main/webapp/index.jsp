<%@page import="cl.ciisa.productos.model.dao.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="cl.ciisa.productos.model.entities.Producto"%> 
<%@page import="cl.ciisa.productos.controller.ProductoController"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css"/>
        <title>Mantenedor de Productos</title>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <%%>
            <div class="mx-auto order-0">
                <a class="navbar-brand mx-auto" href="#"><h1>Mantenedor de Productos (REST)</h1></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <br>
        <div class="container-fluid h-100">
            <br>
            <a href="${baseURL}/api/openapi-ui" class="btn btn-primary stretched-link">Documentación OpenAPI</a>
            
        </div>
        <footer class="fixed-bottom">
            <div class="container-fluid text-center bg-dark text-white">
                <span>Taller Aplicaciones Empresariales - UNI03 - CIISA 2020 ::: Robinson Aceituno</span>
            </div>
        </footer>
    </body>
</html>
